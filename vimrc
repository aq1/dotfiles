" no compatible with the old vi.
set nocompatible                  " Must come first because it changes other options.

" load pathogen plugin manager
filetype off
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()

let mapleader = ","

syntax enable                     " Turn on syntax highlighting.
filetype plugin indent on         " Turn on file type detection.

runtime macros/matchit.vim        " Load the matchit plugin.

set showcmd                       " Display incomplete commands.
set showmode                      " Display the mode you're in.

set backspace=indent,eol,start    " Intuitive backspacing.

set hidden                        " Handle multiple buffers better.
let g:bufExplorerFindActive=0     " Needed by Bufexplorer when hidden is set.

set wildmenu                      " Enhanced command line completion.
set wildmode=list:longest         " Complete files like a shell.

set ignorecase                    " Case-insensitive searching.
set smartcase                     " But case-sensitive if expression contains a capital letter.

set number                        " Show line numbers.
set ruler                         " Show cursor position.

set incsearch                     " Highlight matches as you type.
set hlsearch                      " Highlight matches.

set wrap                          " Turn on line wrapping.
set scrolloff=3                   " Show 3 lines of context around the cursor.

set title                         " Set the terminal's title

set nobackup                      " Don't make a backup before overwriting a file.
set nowritebackup                 " And again.
set directory=$HOME/.vim/tmp//,.  " Keep swap files in one location

set tabstop=2                     " Global tab width.
set shiftwidth=2                  " And again, related.
set expandtab                     " Use spaces instead of tabs

set laststatus=2                  " Show the status line all the time
" Status Line
set statusline=%F%m%r%h%w\
set statusline+=\ [line\ %l\/%L]

if has("eval")
function! SL(function)
  if exists('*'.a:function)
    return call(a:function,[])
  else
    return ''
  endif
endfunction
endif

" Use vividchalk
" colorscheme vividchalk
" colorscheme morning
" let g:solarized_termcolors = 256 " for vim in Mac terminal
colorscheme delek

" Automatically save changes before switching buffer with some
" commands, like :cnfile.
set autowrite

" Tab mappings.
" map <leader>tt :tabnew<cr>
" map <leader>te :tabedit
" map <leader>tc :tabclose<cr>
" map <leader>to :tabonly<cr>
" map <leader>tn :tabnext<cr>
" map <leader>tp :tabprevious<cr>
" map <leader>tf :tabfirst<cr>
" map <leader>tl :tablast<cr>
" map <leader>tm :tabmove

" Uncomment to use Jamis Buck's file opening plugin
" map <Leader>t :FuzzyFinderTextMate<Enter>

" Automatic fold settings for specific files. Uncomment to use.
" autocmd FileType ruby setlocal foldmethod=syntax
" autocmd FileType css  setlocal foldmethod=indent shiftwidth=2 tabstop=2

:vmap <Tab> >gv
:vmap <S-Tab> <gv

" Switch between the 2 last files.
map <leader><leader> <c-^>

" Ctrlp ignore directory
:set wildignore+=tmp/**/*,log/**/*,node_modules/**/*,bower_components/**/*,
          \*/venv/*,
          \*/.git/*,
          \*/mypy_cache/*,
          \*/pytest_cache/*,
          \*/htmlcov/*,
          \*\.egg-info/*

" formatting for less
au BufNewFile,BufRead *.less set filetype=css

" reload automatically the page when outside changes
set autoread

" no line bigger than 80 characters
set colorcolumn=80

" splits shortcuts
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" search and replace on the whole git repo (only works without spaces)
" nnoremap <leader>r :GlobalReplace
" command -nargs=1 GlobalReplace call SearchAndReplace(<f-args>)
" function SearchAndReplace(replacee)
"   let currentword = expand("<cword>")
"   let command = "sed -i '' 's|" . currentword . "|" . a:replacee . "|g' `ack -l " . currentword . "`"
"   echo command
"   let result = system(command)
" endfunction

" Use ,d on top of a word to look it up in Dictionary.app
nmap <silent> <Leader>d :!open dict://<cword><CR><CR>

" Clear search
nmap <silent> ,/ :let@/=""<CR>

" Extend vim expand region
call expand_region#custom_text_objects({
  \ 'a]' :1,
  \ 'ab' :1,
  \ 'aB' :1,
  \ 'ii' :0,
  \ 'ai' :0,
  \ })

" Vim YankStack
nmap <C-p> <Plug>yankstack_substitute_older_paste
nmap <C-P> <Plug>yankstack_substitute_newer_paste

" Send to mac clipboard
vnoremap <C-c> :w !pbcopy<CR><CR>
noremap <C-v> :r !pbpaste<CR><CR>

" Syntastic
let g:syntastic_check_on_open = 1
let g:syntastic_filetype_map = { "html.handlebars": "", "handlebars": "" }
" let g:syntastic_handlebars_checkers=[""]
let g:syntastic_ruby_checkers=["mri"]
let g:syntastic_javascript_checkers=["eslint"]
let g:syntastic_mode_map = { "mode": "active",
                           \ "active_filetypes": ["python"],
                           \ "passive_filetypes": ["scala"] }
let g:syntastic_filetype_map = {"Dockerfile": "dockerfile"}

" Use ,e to edit file relative to current file
nnoremap <Leader>e :e <C-R>=expand('%:p:h') . '/'<CR>

" Grammalecte, French garmmar tool, configuration
let g:grammalecte_cli_py='~/dotfiles/vim/grammalecte/grammalecte-cli.py'
let g:gammalecte_disable_rules='apostrophe_typographique apostrophe_typographique_après_t espaces_début_ligne espaces_milieu_ligne espaces_fin_de_ligne typo_points_suspension1 typo_tiret_incise nbsp_avant_double_ponctuation nbsp_avant_deux_points nbsp_après_chevrons_ouvrants nbsp_avant_chevrons_fermants1 unit_nbsp_avant_unités1 unit_nbsp_avant_unités2 unit_nbsp_avant_unités3'
let g:grammalecte_win_height='10'
nnoremap <Leader>g :GrammalecteCheck<cr>
nnoremap <Leader>gc :GrammalecteClear<cr>

" Tagbar
nmap <F8> :TagbarToggle<CR>

" Vim-Airline Configuration
let g:airline_section_a = ''
let g:airline_theme = 'solarized'
let g:airline_solarized_bg='dark'
let g:airline_powerline_fonts = 0

" Nvim-R
let R_assign = 2

" Get os name
if !exists("g:os")
    if has("win64") || has("win32") || has("win16")
        let g:os = "Windows"
    else
        let g:os = substitute(system('uname'), '\n', '', '')
    endif
endif

" Sharing clipboard on linux
if g:os == "Linux"
    set clipboard=unnamedplus
    map <silent> <F11> :call system("wmctrl -ir " . v:windowid . " -b toggle,fullscreen")<CR>
endif

" Python config
" PEP8 conf
autocmd Filetype python setlocal textwidth=100 shiftwidth=4 tabstop=4 expandtab softtabstop=4 shiftround autoindent
let g:syntastic_python_checkers=["flake8"]
" Run Flake8 when saving
" autocmd BufWritePost *.py call Flake8()
" let g:flake8_show_in_file=0
" let g:flake8_show_in_gutter=1 
" nnoremap <leader>q :cclose<Enter>:sign unplace *<Enter>

" HQL script
au BufRead,BufNewFile *.hql set filetype=sql
let g:syntastic_sql_checkers=[]

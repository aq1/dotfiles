#!/bin/bash

# Install script on Ubuntu

# Run with:
# /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/aq/dotfiles/HEAD/ubuntu_install.sh)"

set -e
set -x
sudo apt -y update
sudo apt -y upgrade
mkdir -p ~/projects
touch ~/.env

# Downloading the repo

sudo apt -y install git
[[ -d ~/dotfiles ]] || git clone https://gitlab.com/aq1/dotfiles.git ~/dotfiles

# Installing main apps...

sudo apt -y install \
  vim curl \
  git tig \
 tree \
  ack \
  vim-gtk3 \
  python3 python3-pip \
  keepassx \
  libssl-dev libreadline-dev zlib1g-dev \
  ubuntu-restricted-extras \
  universal-ctags \
  git-cola \

pip3 install awscli virtualenv autoenv

# Installing Chrome
if (( $(apt list --installed | grep -ic google-chrome-stable) == 0 )) ; then
  cd ~/Downloads
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo apt install -y ./google-chrome-stable_current_amd64.deb
  sudo apt install -y google-chrome-stable
fi

# Installing Docker
sudo apt -y install \
  apt-transport-https \
  ca-certificates \
  gnupg-agent \
  software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt -y update
sudo apt -y install docker-ce docker-ce-cli containerd.io

# Installing docker compose
# sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose

# Setup new ssh key...
if [ ! -f ~/.ssh/id_rsa.pub ]
then
    echo "Generating ssh key"
    mkdir -p ~/.ssh
    ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa
fi
echo "Add your public key to Gitlab, Github, ..."
cat ~/.ssh/id_rsa.pub 
read -n 1 -s -r -p "Press any key to continue"

# Dotfiles submodules
cd ~/dotfiles
if [ ! -f ~/dotfiles/vim/bundle/ack.vim/README.md ]
then
    echo "Initializing dotfiles submodules."
    git submodule init
    git submodule update --init --recursive
    git submodule update
else
    echo "Not initializing dotfiles submodules."
fi
cd ~

# Installing dotfiles...
ln -fs ~/dotfiles/bashrc ~/.bashrc
ln -fs ~/dotfiles/ackrc ~/.ackrc
ln -fs ~/dotfiles/gitconfig ~/.gitconfig
ln -fs ~/dotfiles/gitignore ~/.gitignore
ln -fs ~/dotfiles/vimrc ~/.vimrc
ln -fs ~/dotfiles/vim ~/.vim
ln -fs ~/dotfiles/gvimrc ~/.gvimrc
ln -fs ~/dotfiles/eslintrc ~/.eslintrc
ln -fs ~/dotfiles/pylintrc ~/.pylintrc
ln -fs ~/dotfiles/tmux.conf ~/.tmux.conf

# Installing ruby last version...
if [ ! -d ~/.rbenv ]
then
    echo "Installing rbenv"
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    mkdir -p ~/.rbenv/plugins
    git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
fi
cd ~/.rbenv
src/configure
make -C src
if [ $? -eq 0 ] ; then ; echo "Passing make" ; fi
~/.rbenv/bin/rbenv install 3.0.2
~/.rbenv/bin/rbenv global 3.0.2

# IDE
# sudo snap install pycharm-community --classic
# sudo snap install intellij-idea-community --classic

# Download last Terraform bin from main site
if [ ! -f /usr/local/bin/terraform ]
  cd ~/Downloads
  wget https://releases.hashicorp.com/terraform/1.0.9/terraform_1.0.9_linux_amd64.zip
  unzip terraform_1.0.9_linux_amd64.zip
  sudo mv terraform /usr/local/bin/
  rm terraform_1.0.9_linux_amd64.zip
else
  echo "terraform already installed"
fi

# Cleanup
sudo apt -y autoremove

# Gnome settings
gsettings set org.gnome.desktop.interface cursor-size 36

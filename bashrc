# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
if [[ -n "$PS1" ]]; then

  # don't put duplicate lines in the history. See bash(1) for more options
  # ... or force ignoredups and ignorespace
  HISTCONTROL=ignoredups:ignorespace

  # append to the history file, don't overwrite it
  shopt -s histappend

  # for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
  HISTSIZE=10000
  HISTFILESIZE=20000

  # check the window size after each command and, if necessary,
  # update the values of LINES and COLUMNS.
  shopt -s checkwinsize

  # make less more friendly for non-text input files, see lesspipe(1)
  [ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

  # set variable identifying the chroot you work in (used in the prompt below)
  if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
      debian_chroot=$(cat /etc/debian_chroot)
  fi

  # set a fancy prompt (non-color, unless we know we "want" color)
  case "$TERM" in
      xterm-color) color_prompt=yes;;
  esac

  # uncomment for a colored prompt, if the terminal has the capability; turned
  # off by default to not distract the user: the focus in a terminal window
  # should be on the output of commands, not on the prompt
  force_color_prompt=yes

  if [ -n "$force_color_prompt" ]; then
      if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
      else
    color_prompt=
      fi
  fi

  if [ "$color_prompt" = yes ]; then
      PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
  else
      PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
  fi
  unset color_prompt force_color_prompt

  # If this is an xterm set the title to user@host:dir
  case "$TERM" in
  xterm*|rxvt*)
      PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
      ;;
  *)
      ;;
  esac

  # enable color support of ls and also add handy aliases
  if [ -x /usr/bin/dircolors ]; then
      test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
      alias ls='ls --color=auto'
      #alias dir='dir --color=auto'
      #alias vdir='vdir --color=auto'

      alias grep='grep --color=auto'
      alias fgrep='fgrep --color=auto'
      alias egrep='egrep --color=auto'
  fi

  # Alias definitions.
  # You may want to put all your additions into a separate file like
  # ~/.bash_aliases, instead of adding them here directly.
  # See /usr/share/doc/bash-doc/examples in the bash-doc package.

  if [ -f ~/dotfiles/bash/aliases ]; then
      . ~/dotfiles/bash/aliases
  fi

  # enable programmable completion features (you don't need to enable
  # this, if it's already enabled in /etc/bash.bashrc and /etc/profile
  # sources /etc/bash.bashrc).
  if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
      . /etc/bash_completion
  fi

  # load the other files
  . ~/dotfiles/bash/env
  . ~/dotfiles/bash/config_ps1

  # end of the 'load if interactive'
fi

# Custom bin path
export PATH="$HOME/bin:$PATH"
export PATH="/usr/local/bin:$PATH"


# git autocomplete for gco alias
complete -o default -o nospace -F _git_branch gco

# auto-aliasing to get back to my project
export CDPATH=.:~:~/projects:/media

### General settings
export EDITOR=vim
set -o vi 

### load env variables
. ~/.env

### Load autoenv
if [[ $PLATFORM != 'Darwin' ]] ; then
  # Ubuntu
  source /home/aqu/.local/bin/activate.sh
else
  # Mac
  source /usr/local/Cellar/autoenv/0.3.0/activate.sh
fi

# Setup default language
export LANG=en_US.UTF-8

export HOMEBREW_NO_ANALYTICS=1

if [[ $PLATFORM != 'Darwin' ]] ; then
  # Ubuntu
  export SPARK_HOME=/opt/spark
  export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin
  export PATH=$HOME/go/bin:$PATH
fi

# Spark
# export PATH="/usr/local/opt/openjdk@11/bin:$PATH"
# export CPPFLAGS="-I/usr/local/opt/openjdk@11/include"
# export JAVA_HOME="/Library/Java/JavaVirtualMachines/openjdk-11.jdk/Contents/Home/"
# export SPARK_HOME="/usr/local/Cellar/apache-spark/3.2.0/libexec"
# export PATH="/usr/local/Cellar/apache-spark/3.2.0/bin:$PATH"
# export PYSPARK_PYTHON="python3"

# psql via libpq
# export PATH="/usr/local/opt/libpq/bin:$PATH"

eval "$(/usr/local/bin/brew shellenv)"

# Load rbenv (after loading brew)
if [ -d "$HOME/.rbenv" ]; then
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
fi


# JAVA

if [[ $PLATFORM == 'Darwin' ]] ; then
  # for mac installed with: brew install openjdk@8
  export PATH="/usr/local/opt/openjdk@8/bin:$PATH"
  export CPPFLAGS="-I/usr/local/opt/openjdk@8/include"
  export JAVA_HOME="/usr/local/opt/openjdk@8/libexec/openjdk.jdk/Contents/Home/"
fi

# aliases

PLATFORM=`uname`

# utility
alias reload='source ~/dotfiles/bash/aliases'
alias ea='vim ~/dotfiles/bash/aliases && reload' # Edit aliases
alias ee='vim ~/dotfiles/bash/env'

# listing and browsing
alias ..='cd ..'
alias cdd="cd -"
alias l='ls -CFhl'

# Git
alias gb='git branch -v'
alias gba='git branch -av'
alias gc='git commit -v'
alias gca='git add . ; git add -u ; gc'
# Commit pending changes and quote all args as message
function gg() { 
    git commit -v -a -m "$*"
}
alias gco='git checkout'
alias gd='git diff'
alias gdm='git diff master'
alias gl='git pull --rebase'
alias gp='git push'
alias grhh='git reset HEAD --hard'
# Set upstream branch on remote origin and push to the current branch on it.
function gpo() {
  local CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  [[ $(git config "branch.$(echo $CURRENT_BRANCH).merge") = '' ]] &&
    gp --set-upstream origin `echo $CURRENT_BRANCH` || gp
}
alias g='git stash list && git status'
alias gf='git fetch --tags --all'
alias gfo='git fetch origin'
alias grc='git rebase --continue'
# get all commits from a particular user
function gluser() { 
  git log --pretty="%H" --author=$1 | while read commit_hash; do git show --oneline --name-only $commit_hash | tail -n+2; done | sort | uniq
}
# pretty git log
alias glog="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

# Remove all .swp .swo files from vim in
# a working directory by git.
alias rmswp="find . -name \*.swp -type f -delete ; find . -name \*.swo -type f -delete "

# editor
if [[ $PLATFORM != 'Darwin' ]] ; then
  # Ubuntu
  alias v="gvim "
else
  # Mac
  alias v="mvim "
fi

# Ruby
alias bx='bundle exec'
alias brake='bundle exec rake'
alias bard='bundle exec guard'
alias rs='bx rails server'
alias rc='bx rails console'
alias rg='bx rails generate'
alias rr='.. ; cdd'
alias h='heroku'

# open gem with `bundle open activerecord`
export BUNDLER_EDITOR=vim

alias fullspec="rr ; bundle ; RAILS_ENV=test brake db:drop db:create db:structure:load ; bx rspec"

alias pgstart="pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start"
alias pglog="tail -f /usr/local/var/postgres/server.log"

alias ssh_proxy_box="ssh -i ~/.ssh/aqu-key.pem ubuntu@x.x.x.x"
alias tunel_proxy_box="ssh -v -L 3128:localhost:8888 -N -i ~/.ssh/aqu-key.pem ubuntu@x.x.x.x"

proxy_on() {
  echo 'Proxy On'
    sudo networksetup -setwebproxy 'Wi-Fi' localhost 3128
  sudo networksetup -setsecurewebproxy 'Wi-Fi' localhost 3128
}
 
proxy_off() {
  echo 'Proxy Off'
  sudo networksetup -setwebproxystate 'Wi-Fi' off
  sudo networksetup -setsecurewebproxystate 'Wi-Fi' off
}

alias emberclean="echo '>> rm -Rf tmp dist bower_components node_modules' && rm -Rf tmp dist bower_components node_modules && echo '>> npm install' && npm install && echo '>> bower install' && bower install"

function kill-pg-sessions() {
  echo $1
  echo "psql $1 -c 'SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname = \'$1\' ;'"
  psql $1 -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname = '$1' ;"
}

function acka() {
  echo "ack --type-set='all:match:.*' -k $1"
  ack --type-set='all:match:.*' -k $1
}

alias sve="source venv/bin/activate"

alias coreaudio_restart="ps aux | grep 'coreaudio[d]' | awk '{print $2}' | xargs sudo kill"

alias dbmigrate="echo 'brake db:migrate db:schema:dump db:structure:dump' ; brake db:migrate db:schema:dump db:structure:dump"
alias deploymigrate="echo 'gp heroku' ; gp heroku ; echo 'h run rake db:migrate' ; h run 'rake db:migrate'"

# daily update
if [[ $PLATFORM != 'Darwin' ]] ; then
  # Ubuntu
  alias dailyupdate="sudo apt update -y && \
    sudo apt upgrade && \
    pip3 install --upgrade awscli pip"
else
  # Mac
  alias dailyupdate="softwareupdate -i -a && \
    brew update && \
    brew upgrade && \
    pip3 install --upgrade awscli pip"
fi

alias cleardnscache="sudo killall -HUP mDNSResponder"

alias aws-whoami="aws sts get-caller-identity"

if [[ $PLATFORM == 'Darwin' ]] ; then
  # Mac
  alias google_dns_get='networksetup -getdnsservers Wi-Fi'
  alias google_dns_set='networksetup -setdnsservers Wi-Fi 8.8.8.8 8.8.4.4 ; google_dns_get'
  alias google_dns_clear='networksetup -setdnsservers Wi-Fi empty ; google_dns_get'
fi

alias stop_mbp_gpu="sudo pmset -a gpuswitch 0 && pmset -g | grep gpuswitch && echo 'GPU switched off until next restart.'"

if [[ $PLATFORM != 'Darwin' ]] ; then
  # Ubuntu
  alias open='xdg-open'
fi

alias portainer="sudo docker run -d -p 9000:9000 --name=portainer -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer --no-analytics "

# WMF
alias scap="python3 ../scap/bin/scap"
alias yarn_ui_test="ssh -N stat1004.eqiad.wmnet -L 8088:an-test-master1001.eqiad.wmnet:8088"
alias airflow_ui_test="ssh -t -N -L8600:localhost:8600 an-test-client1001.e"
alias airflow_ui="ssh -t -N -L8600:127.0.0.1:8600 an-launcher1002.e"
alias airflow_ui_aqu="ssh -t -N -L48080:localhost:48080 stat1004.e"

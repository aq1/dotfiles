" Example Vim graphical configuration.
" Copy to ~/.gvimrc or ~/_gvimrc.

let g:osname = substitute(system('uname'), '\n', '', '')
if g:osname == "Darwin"
    set guifont=Monaco:h13
elseif g:osname == "Linux"
    set guifont=Ubuntu\ Mono\ 13
endif
set antialias
set encoding=utf-8                " Use UTF-8 everywhere.
set lines=53 columns=179          " Window dimensions.
set guioptions=''
colorscheme solarized
set background=dark

echo "\n\nInstalling Howbrew itself..."
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "\n\nInstalling with Howbrew..."
brew install vlc vienna transmission github google-chrome \
  google-drive keepassx kindle libreoffice skype \
  firefox atom calibre gimp disk-inventory-x \
  docker wechat whatsapp macvim android-file-transfer \
  microsoft-teams zoom toggl-track


brew install --HEAD universal-ctags/universal-ctags/universal-ctags

brew install tmux rbenv ruby-build python3 ack git gist tig \
  ack gnu-sed sbt gnuplot scala apache-spark openjpeg \
  openssl libpng terraform docker-compose htop \
  tesseract-lang exiftool imagemagick tig tmux tree python \
  wget rbenv libffi ocrmypdf bzip2 intellij-idea-ce

# Last autoenv version: # https://github.com/inishchith/autoenv/issues/188
brew install autoenv
pip3 install virtualenv

echo "\n\nSetup new ssh key..."
ssh-keygen -t rsa -b 4096
cat ~/.ssh/id_rsa.pub 
echo "  => Put it where you need it."

echo "\n\nInstalling dotfiles..."
cd ~/
touch .env
git clone git@github.com:aq/dotfiles.git
ln -s dotfiles/bashrc .bash_profile
ln -s dotfiles/ackrc .ackrc
ln -s dotfiles/gitconfig .gitconfig
ln -s dotfiles/gitignore .gitignore
ln -s dotfiles/vimrc .vimrc
ln -s dotfiles/vim .vim
ln -s dotfiles/gvimrc .gvimrc
ln -s dotfiles/eslintrc .eslintrc
ln -s dotfiles/pylintrc .pylintrc
ln -s dotfiles/tmux.conf .tmux.conf
cd dotfiles
git submodule init 
git submodule update

echo "\n\nInstalling ruby last version..."
rbenv install 3.0.3
rbenv global 3.0.3


# Update bash
brew install bash
echo '/opt/homebrew/bin/bash' | sudo tee -a /etc/shells
chsh -s /opt/homebrew/bin/bash

# Also check: iCloud, iTunes, iBooks, Google drive

pip3 install flake8 autopep8 jupyter jupyter_kernel_gateway
mkdir ~/.atom
ln -s dotfiles/atom.cson ~/atom/config.cson
ln -s dotfiles/atom_package.list  ~/.atom/package.list
apm install --packages-file ~/.atom/package.list
# or
apm install linter linter-flake8 autocomplete-python python-autopep8 script atom-file-icons atom-material-syntax minimap minimap-git-diff minimap-highlight-selected jupyter-notebook gitlab

# Spark
brew install java11
sudo ln -sfn /usr/local/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
java -version
brew install scala apache-spark
# install zeppelin from git repo
